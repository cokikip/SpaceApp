jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IMission, Mission } from '../mission.model';
import { MissionService } from '../service/mission.service';

import { MissionRoutingResolveService } from './mission-routing-resolve.service';

describe('Service Tests', () => {
  describe('Mission routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: MissionRoutingResolveService;
    let service: MissionService;
    let resultMission: IMission | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(MissionRoutingResolveService);
      service = TestBed.inject(MissionService);
      resultMission = undefined;
    });

    describe('resolve', () => {
      it('should return IMission returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultMission = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultMission).toEqual({ id: 123 });
      });

      it('should return new IMission if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultMission = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultMission).toEqual(new Mission());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultMission = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultMission).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
